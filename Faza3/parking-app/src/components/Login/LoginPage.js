import React, { Component } from 'react';
import LoginStore from './LoginStore'
import Login from './Login'
import './Login.css';

import {BrowserRouter as Router, Redirect, Link, NavLink} from "react-router-dom";
import {Route} from "react-router-dom/Route";
import MapContainer from '../Map/Map'


class LoginPage extends React.Component {
   
 
  constructor(props){
    
    super(props)
      this.store= new LoginStore()
      this.check=(user)=>{
          this.store.checkUser(user) 
      }
      this.mainPage=false
   
  }
 
  componentDidMount(){
      this.store.emitter.addListener('LOGIN_SUCCESS',()=>{
      document.getElementById('nav-link').click();
      console.log('Login successfull');
    })
    this.store.emitter.addListener('LOGIN_ERROR',()=>{
     alert("Datele intorduse nu sunt corecte!");
    })
    
  }
  render() {
    return (
        <div id="login_container" > 
           <Login checkUser={this.check} />
        </div>
  
    );
  }
}
 


export default LoginPage;
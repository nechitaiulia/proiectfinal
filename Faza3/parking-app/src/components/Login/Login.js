import React, { Component } from 'react';
import './Login.css';
import {BrowserRouter as Router, Redirect,withRouter,NavLink} from "react-router-dom";
import {Route} from "react-router-dom/Route";
import {Button,Label} from 'react-bootstrap'
class Login extends Component {
  
  constructor(props){
      super(props)
      this.state={
          email:'',
          password:''
      }
      
      this.handleOnClick=()=>{
        this.props.checkUser({
        email:this.state.email,
        password:this.state.password
        })
      
      }
    
        
   
      this.handleChange=(evt)=>{
          this.setState({
              [evt.target.name]:evt.target.value
          })
      }
  }
  
  render() {
   
    return (
      <div id="login_div"> 
          <form id="login_f" >
            <div id="email_div">
               <div> <label id="label_email">Email</label></div>
                <input type="email" id="l_email" name="email" onChange={this.handleChange}/>
            </div>
            <div id="password_div">
                <div>
                  <label >Password</label></div>
                <div>
                  <input type="password" id="l_password" name="password" onChange={this.handleChange}/>
                </div>
            </div>
            <div id="button_div">
                <Button bsStyle="danger" id="l_login" onClick={this.handleOnClick}>Login</Button> 
            </div>
           <NavLink id="nav-link" to="/main" exact activeStyle={
                {color: 'red'}
                }>Main Page</NavLink>
           </form> 
      </div>
    );
  }
}

export default Login;
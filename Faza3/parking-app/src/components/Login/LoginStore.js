import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER='http://proiectfinal-nechiulia.c9users.io:8080'
class LoginStore{
    
    constructor(){
        this.content=[]
        this.emitter= new EventEmitter()
    }
    async checkUser(user){
        try{
            let response = await axios.post(`${SERVER}/login`,user) 
            
            this.emitter.emit('LOGIN_SUCCESS')
        }
        catch(ex){
            console.warn(ex)
            this.emitter.emit('LOGIN_ERROR')
        }
}
    async addUser(user){
        try{
            let response = await axios.post(`${SERVER}/register`,user) 
            this.emitter.emit('REGISTER_SUCCESS')
        }
        catch(ex){
            console.warn(ex)
            this.emitter.emit('REGISTER_ERROR')
        }
    }
} 
export default LoginStore
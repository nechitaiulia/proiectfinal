import React, { Component } from 'react';
import CheckinStore from './CheckinStore.js'
import CheckinForm from './CheckinForm.js'

import {BrowserRouter as Router, Redirect, Link, NavLink} from "react-router-dom";
import {Route} from "react-router-dom/Route";



class CheckinPage extends React.Component {
   
 
  constructor(props){
    
    super(props)
     this.state={
        id_checkin:''
      }
      this.store= new CheckinStore()
      this.addCheckin=(checkin)=>{
          this.store.addCheckin(checkin) 
      }
      this.checkOut=(carNumber)=>{
          this.store.checkOut(carNumber) 
      }
     
     
  }
  
  componentDidMount(){
      this.store.emitter.addListener('ADD_CHECKIN_SUCCESS',()=>{
      console.log('Checkin added successfull');
       alert('Checkin successfull');
      document.getElementById("add-car-nav-link").click();
    })
      /*this.store.emitter.addListener('ADD_CHECKIN_ERROR',()=>{
     alert("Datele introduse nu sunt corecte!");
    })*/
     this.store.emitter.addListener('CHECKOUT_SUCCESS',()=>{
      console.log('Checkout successfull');
      alert('Checkout successfull');
      document.getElementById("add-car-nav-link").click();

    })


    
  }
  render() {
    return (
        <div> 
           <CheckinForm addCheckin={this.addCheckin} checkOut={this.checkOut} />
        </div>
  
    );
  }
}
 


export default CheckinPage;
import React, { Component } from 'react';
import {Button} from 'react-bootstrap'
import './Checkin.css';
class CheckinForm extends Component {
  
  constructor(props){
      super(props)
      this.state={
          parkingname:'',
          carnumber:'',
          checkinAdded:false
      }
      
      this.handleOnClick=()=>{
        
        if(!this.state.checkinAdded){
        this.props.addCheckin({
        parkingname:this.state.parkingname,
        carnumber:this.state.carnumber
        })
         this.state.checkinAdded=true;
        //document.getElementById("checkIn").value="Check out"
        document.getElementById("h-adauga-checkin").value="Apasă check-out când părăsești parcarea!"
        }
       /* else {
       
        this.props.checkOut(this.state.carnumber)
        this.state.checkinAdded=false;
        document.getElementById("checkInOut").value="Check in"
        }*/
     
      }
this.handleOnClickOUT=()=>{
      
        this.props.checkOut(this.state.carnumber)
        this.state.checkinAdded=false;
        //document.getElementById("checkIn").value="Check in"
      
      }
      this.handleChange=(evt)=>{
          this.setState({
              [evt.target.name]:evt.target.value
          })
      }
  }
  render() {
   
    return (
      <div id="checkin_div"> 

      <form id="checkin_f" >
      
       <input type="text" placeholder="Nume parcare" id="checkin_parking_address" name="parkingname" onChange={this.handleChange}/>
       <input type="text" placeholder="Numar masina" id="checkin_car_number" name="carnumber" onChange={this.handleChange}/>
       <Button bsStyle="danger" id="checkIn" onClick={this.handleOnClick}>Check In </Button>
      <Button bsStyle="danger" id="checkOut" onClick={this.handleOnClickOUT}>Check Out</Button>
       </form> 
      </div>
    );
  }
}

export default CheckinForm;
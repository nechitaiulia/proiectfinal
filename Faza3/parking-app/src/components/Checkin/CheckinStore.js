import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER='http://proiectfinal-nechiulia.c9users.io:8080'
class CheckinStore{
    
    constructor(){
        this.content=0;
        this.emitter= new EventEmitter()
    }
    async addCheckin(checkin){
        try{
            let response = await axios.post(`${SERVER}/addCheckin`,checkin) 
            this.emitter.emit('ADD_CHECKIN_SUCCESS')
        }
        catch(ex){
            console.warn(ex)
            this.emitter.emit('ADD_CHECKIN_ERROR')
        }
}

async checkOut(registration_number){
     
        try{
            let response = await axios.delete(`${SERVER}/checkin/delete/${registration_number}`) 
            this.emitter.emit('CHECKOUT_SUCCESS')
           
        }
        catch(ex){
            console.warn(ex)
            this.emitter.emit('CHECKOUT_ERROR')
        }
}

   
} 
export default CheckinStore
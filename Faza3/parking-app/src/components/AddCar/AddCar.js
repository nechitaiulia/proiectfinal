import React, { Component } from 'react';

import {BrowserRouter as Router, Redirect,withRouter,NavLink} from "react-router-dom";
import {Route} from "react-router-dom/Route";
import {Button,Label} from 'react-bootstrap'
import './AddCar.css'
class AddCar extends Component {
  
  constructor(props){
      super(props)
      this.state={
        add_car_email:'',
          reg_number:'',
          type_car:''
      }
      
      this.handleOnClick=()=>{
        this.props.addCar({
        email:this.state.add_car_email,
        registration_number:this.state.reg_number,
        type_car:this.state.type_car
        })
      
      }
   
      this.handleChange=(evt)=>{
          this.setState({
              [evt.target.name]:evt.target.value
          })
      }
  }
  
  render() {
   
    return (
      <div id="login_div"> 
      <h4>Introdu datele pentru ați adăuga mașina în baza de date!</h4>
          <form id="login_f" >
          <div id="email_div">
               <div> <label id="label_email">Email</label></div>
                <input type="text" id="add_car_email" name="add_car_email" onChange={this.handleChange}/>
            </div>
            <div id="registration_number_div">
               <div> <label id="label_registration_number">Registration number</label></div>
                <input type="text" id="reg_number" name="reg_number" onChange={this.handleChange}/>
            </div>
            <div id="type_car_div">
                <div>
                  <label >Type of the car</label></div>
                <div>
                  <input type="text" id="type_car" name="type_car" onChange={this.handleChange}/>
                </div>
            </div>
            <div id="button_div">
                <Button bsStyle="danger" id="add_car" onClick={this.handleOnClick}>Add car</Button> 
            </div>
           <NavLink id="add-car-nav-link" to="/main" exact activeStyle={
                {color: 'red'}
                }>Main Page</NavLink>
           </form> 
      </div>
    );
  }
}

export default AddCar;
import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER='http://proiectfinal-nechiulia.c9users.io:8080'
class AddCarStore{
    
    constructor(){
        this.content=[]
        this.emitter= new EventEmitter()
    }
    async addCar(data){
        try{
            let response = await axios.post(`${SERVER}/user/addcar`,data) 
            this.emitter.emit('ADD_CAR_SUCCESS')
        }
        catch(ex){
            console.warn(ex)
            this.emitter.emit('ADD_CAR_ERROR')
        }
}
} 
export default AddCarStore
import React, { Component } from 'react';
import AddCarStore from './AddCarStore.js'
import AddCar from './AddCar.js'
import './AddCar.css';

import {BrowserRouter as Router, Redirect, Link, NavLink} from "react-router-dom";
import {Route} from "react-router-dom/Route";

class AddCarPage extends React.Component {
   
 
  constructor(props){
    
    super(props)
     
      this.store= new AddCarStore()
      this.addCar=(car)=>{
          this.store.addCar(car) 
      }
     
     
     
  }
  
  componentDidMount(){
      this.store.emitter.addListener('ADD_CAR_SUCCESS',()=>{
    document.getElementById('add-car-nav-link').click();
    alert("The car was added to database");
      console.log('The car was added to database');
    })
    this.store.emitter.addListener('ADD_CAR_ERROR',()=>{
     alert("Datele introduse nu sunt corecte!");
    })
  
    
  }
  render() {
    return (
        <div id="add_container"> 
           <AddCar addCar={this.addCar} />
        </div>
  
    );
  }
}
 


export default AddCarPage;
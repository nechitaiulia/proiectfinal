import React, { Component } from 'react';
import LoginStore from '../Login/LoginStore'
import Register from './Register'
import './Register.css';
import {BrowserRouter as Router,NavLink} from "react-router-dom";
import {Route} from "react-router-dom/Route";
import MapContainer from '../Map/Map'


class RegisterPage extends Component {
   
 
  constructor(props){
    super(props)
  
      this.store= new LoginStore()
    
       this.addUser=(user)=>{
          this.store.addUser(user) 
      }
      
      
  }
 
  
  componentDidMount(){
      
     this.store.emitter.addListener('REGISTER_SUCCESS',()=>{
         document.getElementById('nav-link-register').click();
          console.log('Register successfull');
       })
  }
  render() {
    return (

        <div id="register_container"> 
            <Register addUser={this.addUser}/>
        </div>
      
    );
  }
}
 


export default RegisterPage;
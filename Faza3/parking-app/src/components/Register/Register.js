import React, { Component } from 'react';
import './Register.css';
import {Button} from 'react-bootstrap'

import {BrowserRouter as Router,NavLink} from "react-router-dom";
class Register extends Component {
  constructor(props){
      super(props)
      this.state={
          email:'',
          password:'',
          verifypassword:''
      }
      
      this.handleChange=(evt)=>{
          this.setState({
              [evt.target.name]:evt.target.value
          })
      }
  }
  
  render() {
    return (
      <div id="register_div" > 
          <form id="register_f">
            <div>
                <div>
                    <label id="label_email" >Email</label>
                </div> 
                <div>
                    <input type="email" id="r_email" name="email" onChange={this.handleChange}/>
                </div> 
            </div> 
            <div>
                <div>
                    <label id="label_password" >Password</label>
                </div>
                <div>
                    <input type="password" id="r_password" name="password" onChange={this.handleChange}/>
                </div>
            </div>
            <div>
                <div>
                    <label id="label_chpassword" >Change Password</label>
                </div>
                <div>
                    <input type="password" id="r_verifypassword" name="verifypassword" onChange={this.handleChange}/>
                </div>
            </div>
            <div id="button_div">
                <Button bsStyle="danger" id="r_register" onClick={()=> {
                       if(this.state.password === this.state.verifypassword){
                       this.props.addUser({
                        email:this.state.email,
                        password:this.state.password
                        })
                       }
                        else {
                          alert('Parolele nu sunt identice!');
                        }} 
                       }  >Register</Button>
           </div>
           <NavLink id="nav-link-register" to="/main" exact activeStyle={
            {color: 'red'}
            }>Main Page</NavLink>
       </form> 
      </div>
    );
  }
}

export default Register;
import React from 'react';
import ParkingStore from './ParkingStore'
import './Parking.css'
//import ParkingForm from './ParkingForm'

class ParkingForm extends React.Component {
  constructor(){
    super()
    this.state={
      parkings : []
    }

    this.store = new ParkingStore()

  }
  
  componentDidMount(){
    this.store.getAllParkings()
    this.store.emitter.addListener('GET_ALLPARKINGS_SUCCSES',()=>{
      this.setState({
        parkings : this.store.content
      })
    })
  }
  render() {
    return (
      <div>
          <div id = "id-lista">
                <h1> Disponibilitate </h1>
                <ul >
                <li id="header-disponibilitate" >Nume parcare &nbsp; &nbsp; &nbsp; Numar locuri disponibile</li>
                  
                </ul>
               {
                this.state.parkings.map((e,i)=> <li key={i} >
                {e.name_parking}   &nbsp; &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;{e.available_spots}
                </li>
                )
               }
          </div>
      </div>
    );
  }
}

export default ParkingForm;

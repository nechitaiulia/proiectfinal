import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER ="http://proiectfinal-nechiulia.c9users.io:8080"

class ParkingStore{
    constructor(){
        this.content = []
        this.emitter = new EventEmitter()
    }
    async getAllParkings(){
        try{
            let response = await axios(`${SERVER}/get-all-parkings`)
            this.content = response.data
            this.emitter.emit('GET_ALLPARKINGS_SUCCSES')
        }
        catch(ex){
            console.log(ex)
            this.emitter.emit('GET_ALL_ERROR')
        }
    }
    // async addOne(product){
    //      try{
    //         console.log(product)
    //         await axios.post(`${SERVER}/add`,product)
    //         this.getAll()
    //         this.emitter.emit('ADD_SUCCSES')
            
    //     }
    //     catch(ex){
    //         console.log(ex)
    //         this.emitter.emit('ADD_ERROR')
    //     }
    // }
}


export default ParkingStore;
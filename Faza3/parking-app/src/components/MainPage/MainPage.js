import React, { Component } from 'react';
import './MainPage.css'; 
import {Button} from 'react-bootstrap'
import CheckinPage from '../Checkin/CheckinPage'
import {BrowserRouter as Router, Redirect, Link, NavLink} from "react-router-dom";
import {Route} from "react-router-dom/Route";
import MapContainer from '../Map/Map'
import ParkingForm from '../Parking/ParkingForm'
const geolocation = require('geolocation')

class MainPage extends React.Component {
   
 
  constructor(props){
    
    super(props)
     
  }

 
  render() {
    return (
        <div id="main_page"> 
         <div id="btn_add_car">
         <h4>Adaugăți mașina în baza de date</h4>
              <Button bsStyle="danger" id="go_to_add_car" name="go_to_add_car"  onClick={()=> {
                       document.getElementById("add-new-car-nav-link").click();
                      }} >Add a new car</Button>
              <NavLink id="add-new-car-nav-link" to="/addCar" exact activeStyle={
                {color: 'red'}
                }>Add car Page</NavLink>
           </div>
          <div id="chein_form">
          <h4 id="h-adauga-checkin">Adaugă check-in</h4>
              <CheckinPage/>
           </div>
        
          

           <div id="div-map">
              <MapContainer id="container"/>
           </div>
           <div id="div-disponibilitate">
              <ParkingForm id="form-disponibilitate"/>
           </div>
        </div>
      
    );
    
  }
}
 


export default MainPage;
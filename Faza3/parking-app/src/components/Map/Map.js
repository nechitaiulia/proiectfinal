import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import React from 'react'
import './Map.css'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
const geolocation = require('geolocation')
export class MapContainer extends React.Component {
 
 
    state = {
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: {},
  };
  
  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });
    
     onMapClicked = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      })
    }
  };
 
 
  render() {
     const style = {
            width: '20%',
            height: '20%'
          
      }
      //app.use(cors())
/*      let url = 'https://maps.googleapis.com/maps/api/place/search/json';
        fetch(url,{mode: 'no-cors'})
        .then(res => res.json())
        .then((out) => {
          console.log('Checkout this JSON! ', out);
        })
        .catch(err => { throw err });*/
        
/*        function getPlaces() {
           return fetch('https://maps.googleapis.com/maps/api/place/search/json')
           .then((response) => response.json())
           .then((responseJson) => {
             return responseJson.movies;
           })
           .catch((error) => {
             console.error(error);
           });
        }*/
        
    
    return (
        
      <div id="div-map-cointainer" className="map-cointainer" >
        <div id="div-google-map">
          <Map  id="map"
                 
                initialCenter={{ lat: 44.4240, lng: 26.102538390000063 }}
                zoom={15}
                google={this.props.google}
                onClick={this.onMapClicked}>
        <Marker onClick={this.onMarkerClick}
                name={'Current location'} 
                position={{lat: 44.4267674, lng: 26.102538390000063}}/>
        <Marker onClick={this.onMarkerClick}
                title={'The marker`s title will appear as a tooltip.'}
                name={'Parcare publica-Unirii'}
                
                position={{lat:44.427975, lng: 26.105723}} />
              <Marker onClick={this.onMarkerClick}
                name={'Parcare publica-Militari'}
                position={{lat: 44.4240, lng: 26.102538390000063}} />
                <Marker onClick={this.onMarkerClick}
                name={'Parcare Centru Vechi'}
                position={{lat:44.431291, lng:  26.104339}} />
                 <Marker onClick={this.onMarkerClick}
                name={'Parcare publica-Dristor'}
               
                position={{lat:44.452280, lng:   26.047909}} />
                
        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}>
            <div id="selected-place">
              <h1>{this.state.selectedPlace.name}</h1>
            </div>
        </InfoWindow>
       </Map>
               </div>
        
      </div>
    );
  }
}
 
export default GoogleApiWrapper({
  apiKey: ("AIzaSyBF2zMuRHPqYWd7N8jUVzAUE0MPOjsHuxI")
})(MapContainer)
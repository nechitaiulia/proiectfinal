import React, { Component } from 'react';
// import logo from './components/Login/logo.svg';
import LoginPage from './components/Login/LoginPage'
import RegisterPage from './components/Register/RegisterPage'
import MapContainer from './components/Map/Map'
import MainPage from './components/MainPage/MainPage'
import AddCarPage from './components/AddCar/AddCarPage'


import './App.css';
import {BrowserRouter as Router,Link, NavLink,Redirect} from "react-router-dom";
import Route from "react-router-dom/Route";

  const routes = [
  {
    path: "/home",
    component: MapContainer
  }
  ];

class App extends Component {
  state={
    loggedIn: false
  }
 
  render() {
    return (
        <Router>
        <div id="navbar" className="App"> 
        <ul id="navbar-list">
            <li>
            <NavLink to="/" exact activeStyle={
            {color: 'red'}
            }
            >Home</NavLink>
            </li>
            <li>
             <NavLink to="/login" exact activeStyle={
            {color: 'red'}
            }>Login</NavLink>
             </li>
              <li>
             <NavLink to="/register" exact activeStyle={
            {color: 'red'}
            }>Register</NavLink>
             </li>
             
             </ul>
           
           <Route path="/" exact strict render={
             ()=>{
               return(<div id="div-welcome">
                         <h1 id="h-welcome1" >Welcome to the parking helper </h1>
                        
                         </div>
                      )}
           }/>
         
             <Route path="/login" exact strict component={LoginPage} />
             <Route path="/register" exact strict component={RegisterPage} />
             <Route path="/main" exact strict component={MainPage} />
            <Route path="/addCar" exact strict component={AddCarPage} />
          
            
        </div>
      </Router>
    );
  }
 
 
}

export default App;
